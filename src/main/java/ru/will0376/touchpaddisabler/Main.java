package ru.will0376.touchpaddisabler;

import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinReg;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

	private static boolean enabledTouch;
	private static String key;

	public static void main(String[] args) {
		Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
		logger.setLevel(Level.OFF);

		logger.setUseParentHandlers(false);
		key = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\PrecisionTouchPad\\Status";
		int fromReg = Advapi32Util.registryGetIntValue(WinReg.HKEY_CURRENT_USER, key, "Enabled");
		enabledTouch = fromReg == 1;
		System.out.printf("Enabled Touchpad: %s\n", enabledTouch);
		try {
			GlobalScreen.registerNativeHook();
		} catch (NativeHookException ex) {
			System.exit(1);
		}
		KeyEvent nativeKeyListener = new KeyEvent();
		new Thread(() -> {
			System.out.println("Started Thread");
			while (true) {
				try {
					if (nativeKeyListener.isFn() && nativeKeyListener.isN()) {
						int value = enabledTouch ? 0 : 1;
						toggle(WinReg.HKEY_CURRENT_USER, key, value);
						toggle(WinReg.HKEY_USERS,
								"S-1-5-21-1765119516-279353956-3054051896-1001\\Software\\Microsoft\\Windows" +
										"\\CurrentVersion\\PrecisionTouchPad\\Status", value);
						enabledTouch = !enabledTouch;
						execComma(enabledTouch ? "C:\\Windows\\System32\\schtasks.exe /run /tn \"Apps\\EnableTouchpad\"" :
								"C" + ":\\Windows\\System32\\schtasks.exe /run /tn \"Apps\\DisableTouchpad\"");
						System.out.println("EnabledTouch new value: " + enabledTouch);
						nativeKeyListener.fn = nativeKeyListener.n = false;
					}
					if (nativeKeyListener.isFn() && nativeKeyListener.isEsc()) {
						System.exit(1);
						break;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			System.out.println("stopped While");
		}).start();
		GlobalScreen.addNativeKeyListener(nativeKeyListener);
	}

	private static void toggle(WinReg.HKEY hkey, String key, int value) {
		Advapi32Util.registrySetIntValue(hkey, key, "Enabled", value);
	}

	private static void execComma(String command) throws IOException {
		Runtime.getRuntime().exec(command);
	}

	private static class KeyEvent implements NativeKeyListener {
		public boolean fn = false;
		public boolean n = false;
		public boolean esc = false;

		public synchronized boolean isFn() {
			return fn;
		}

		public synchronized boolean isN() {
			return n;
		}

		public synchronized boolean isEsc() {
			return esc;
		}

		@Override
		public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {
		}

		@Override
		public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
			int keyCode = nativeKeyEvent.getKeyCode();
			int rawCode = nativeKeyEvent.getRawCode();
			if (keyCode == 0 && rawCode == 255) {
				fn = true;
			}
			if (keyCode == 49 && rawCode == 78) {
				n = true;
			}
			if (keyCode == 18 && rawCode == 69) {
				esc = true;
			}
		}

		@Override
		public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {
			int keyCode = nativeKeyEvent.getKeyCode();
			int rawCode = nativeKeyEvent.getRawCode();
			if (keyCode == 0 && rawCode == 255) {
				fn = false;
			}
			if (keyCode == 49 && rawCode == 78) {
				n = false;
			}
			if (keyCode == 18 && rawCode == 69) {
				esc = false;
			}
		}
	}
}
